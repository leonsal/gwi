package webview_ext

import (
	"io/fs"
	"net/http"

	"github.com/webview/webview"
	"gitlab.com/leonsal/gwi"
	"gitlab.com/leonsal/gwi/logger"
	"gitlab.com/leonsal/gwi/staticfs"
)

var log = logger.Default()

func Start(cfg *gwi.ServerConfig, h func(*gwi.Client, bool)) error {

	// Creates HTTP server
	server := &http.Server{
		Addr: cfg.Address,
	}

	log.Info("Webview: using embedded static filesystem")
	var hfs http.FileSystem
	fsys, err := fs.Sub(staticfs.StaticFS, "static")
	if err != nil {
		return err
	}
	hfs = http.FS(fsys)
	http.Handle("/", http.FileServer(hfs))

	// Creates GWI handler
	handler, err := gwi.New(&gwi.Config{MaxClients: cfg.MaxClients}, h)
	if err != nil {
		return err
	}
	http.Handle("/gwi", handler)

	go func() {
		log.Info("listening on:%s", cfg.Address)
		err := server.ListenAndServe()
		if err != nil {
			log.Error("%s", err)
		}
	}()

	// Creates webview
	log.Info("Creating webview")
	debug := true
	w := webview.New(debug)
	defer w.Destroy()
	w.SetTitle("")
	w.SetSize(800, 600, webview.HintNone)

	// Starts webview
	scriptPath := "http://" + cfg.Address
	log.Info("Navigate to:%s", scriptPath)
	w.Navigate(scriptPath)
	w.Run()
	return nil
}
