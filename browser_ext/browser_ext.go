package browser_ext

import (
	"net/http"
	"os"

	"gitlab.com/leonsal/gwi"
	"gitlab.com/leonsal/gwi/logger"
)

var log = logger.Default()

func Start(cfg *gwi.ServerConfig, h func(*gwi.Client, bool)) error {

	// Creates HTTP server
	server := &http.Server{
		Addr: cfg.Address,
	}

	// Sets static file server
	log.Info("Using external static filesystem:%s", cfg.Static)
	hfs := http.FS(os.DirFS(cfg.Static))
	http.Handle("/", http.FileServer(hfs))

	// Creates GWI handler
	handler, err := gwi.New(&gwi.Config{MaxClients: cfg.MaxClients}, h)
	if err != nil {
		return err
	}
	http.Handle("/gwi", handler)

	log.Info("listening on:%s", cfg.Address)
	return server.ListenAndServe()
}
