module gitlab.com/leonsal/gwi

go 1.16

require (
	github.com/gorilla/websocket v1.4.2
	github.com/webview/webview v0.0.0-20210330151455-f540d88dde4e
)
