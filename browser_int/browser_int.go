package browser_int

import (
	"io/fs"
	"net/http"

	"gitlab.com/leonsal/gwi"
	"gitlab.com/leonsal/gwi/logger"
	"gitlab.com/leonsal/gwi/staticfs"
)

var log = logger.Default()

func Start(cfg *gwi.ServerConfig, h func(*gwi.Client, bool)) error {

	// Creates HTTP server
	server := &http.Server{
		Addr: cfg.Address,
	}

	log.Info("Using embedded static filesystem")
	var hfs http.FileSystem
	fsys, err := fs.Sub(staticfs.StaticFS, "static")
	if err != nil {
		return err
	}
	hfs = http.FS(fsys)
	http.Handle("/", http.FileServer(hfs))

	// Creates GWI handler
	handler, err := gwi.New(&gwi.Config{MaxClients: cfg.MaxClients}, h)
	if err != nil {
		return err
	}
	http.Handle("/gwi", handler)

	log.Info("listening on:%s", cfg.Address)
	return server.ListenAndServe()
}
