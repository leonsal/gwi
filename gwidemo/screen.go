package main

import (
	"fmt"

	"gitlab.com/leonsal/gwi"
)

var screenConfig = gwi.Obj{
	"view": "layout",
	"rows": gwi.Array{mainMenu, toolBar, tabView},
}

var mainMenu = gwi.Obj{
	"view":      "menu",
	"id":        "mainmenu",
	"autowidth": true,
	"type": gwi.Obj{
		"subsign": false,
	},
	"data": gwi.Array{
		gwi.Obj{
			"value": "File",
			"submenu": gwi.Array{
				gwi.Obj{"value": "Logout", "id": "logout"},
				gwi.Obj{"value": "Shutdown", "id": "shutdown"},
			},
		},
		gwi.Obj{
			"value": "Demos",
			"submenu": gwi.Array{
				gwi.Obj{"value": "Demo1", "id": "demo1"},
				gwi.Obj{"value": "Demo2", "id": "demo2"},
			},
		},
	},
}

var toolBar = gwi.Obj{
	"view": "toolbar",
	"id":   "mainToolbar",
	"elements": gwi.Array{
		gwi.Obj{
			"view":    "richselect",
			"id":      "tb.select",
			"width":   240,
			"align":   "left",
			"tooltip": "Select wireless interface",
			"options": gwi.Array{
				gwi.Obj{"id": 1, "value": "Item 1"},
				gwi.Obj{"id": 2, "value": "Item 2"},
				gwi.Obj{"id": 3, "value": "Item 3"},
			},
			"on": gwi.Obj{
				"onChange": onSelectChange,
			},
		},
		gwi.Obj{
			"view":     "button",
			"value":    "B1",
			"align":    "left",
			"tooltip":  "Button 1",
			"width":    40,
			"disabled": false,
			"click": func(c *gwi.Client, e *gwi.MsgEvent) {
				log.Debug("toolbar b1")
			},
		},
		gwi.Obj{
			"view":     "button",
			"id":       "btn2",
			"value":    "B2",
			"align":    "left",
			"tooltip":  "Button 2",
			"width":    40,
			"disabled": false,
		},
	},
}

var tabView = gwi.Obj{
	"view": "tabview",
	"id":   "main.tabview",
	"type": "clean",
	"css":  "tabview",
	"tabbar": gwi.Obj{
		"height":      28,
		"close":       false,
		"optionWidth": 210,
	},
	"cells": gwi.Array{logTabPage},
}

var logTable = gwi.Obj{
	"view":         "datatable",
	"id":           "log.dtable",
	"resizeColumn": true,
	"select":       "row",
	"navigation":   true,
	"footer":       false,
	"columns": gwi.Array{
		gwi.Obj{
			"id":     "Time",
			"header": "Time",
			//"format":     util.fmtDateTime,
			"sort":      "int",
			"fillspace": 2,
		},
		gwi.Obj{
			"id":     "Level",
			"header": "Level",
			//"format":     formatLogLevel,
			"sort":      "int",
			"fillspace": 1,
		},
		gwi.Obj{
			"id":        "Usermsg",
			"header":    "Message",
			"fillspace": 16,
		},
	},
}

var logTabPage = gwi.Obj{
	"header": "Log",
	"close":  true,
	"width":  60,
	"body": gwi.Obj{
		"view": "layout",
		"id":   "log.view",
		"rows": gwi.Array{logTable},
	},
}

func onSelectChange(c *gwi.Client, ev *gwi.MsgEvent) {

	fmt.Println("onSelectChange")
}
