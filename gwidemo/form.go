package main

import (
	"encoding/json"
	"fmt"

	"gitlab.com/leonsal/gwi"
)

var first = true

func formStart(c *gwi.Client) {

	if first {
		err := c.Ui(formConfig)
		if err != nil {
			log.Error("%s", err)
			return
		}
		first = false
	}
	log.Info("SHOW FORM--------------")
	c.View("win").Call("show")
}

var formConfig gwi.Obj = gwi.Obj{
	"view":     "window",
	"id":       "win",
	"modal":    true,
	"position": "center",
	"head":     "Form",
	"width":    420,
	"body": gwi.Obj{
		"view":   "form",
		"id":     "win.form",
		"scroll": false,
		"elements": gwi.Array{
			gwi.Obj{
				"view":     "text",
				"label":    "User:",
				"name":     "user",
				"required": true,
			},
			gwi.Obj{
				"view":     "text",
				"label":    "Password:",
				"name":     "password",
				"type":     "password",
				"required": true,
			},
			gwi.Obj{
				"margin": 5,
				"cols": gwi.Array{
					gwi.Obj{
						"view":  "button",
						"id":    "login.form.submit",
						"value": "Submit",
						"type":  "form",
						"click": formOnSubmit,
					},
					gwi.Obj{
						"view":  "button",
						"id":    "login.form.cancel",
						"value": "Quit",
						"type":  "form",
						"click": formOnCancel,
					},
				},
			},
		},
		"elementsConfig": gwi.Obj{
			"labelAlign": "right",
			"labelWidth": 100,
		},
	},
}

func formOnSubmit(c *gwi.Client, ev *gwi.MsgEvent) {

	values, _ := c.View("win.form").Call("getValues")
	type loginResp struct {
		User     string
		Password string
	}
	var resp loginResp
	err := json.Unmarshal([]byte(values), &resp)
	if err != nil {
		log.Error("error: %v decoding message", err)
		return
	}
	if resp.User != "demo" || resp.Password != "demo" {
		c.Promise("webix.alert", gwi.Obj{
			"title": "Error",
			"ok":    "OK",
			"text":  "Invalid user or password. User 'demo' and 'demo'",
		}, nil)
	} else {
		c.Promise("webix.alert", gwi.Obj{
			"title": "Success",
			"ok":    "OK",
			"text":  "Form validated OK!",
		}, func(c *gwi.Client, res string) { c.View("win").Call("hide") })
	}
	fmt.Printf("VALUES:%+v/\n", resp)
}

func formOnCancel(c *gwi.Client, ev *gwi.MsgEvent) {

	c.View("win").Call("hide")
}
