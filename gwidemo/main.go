package main

import (
	"flag"

	"gitlab.com/leonsal/gwi"
	server "gitlab.com/leonsal/gwi/browser_ext"
	"gitlab.com/leonsal/gwi/logger"
)

var (
	opAddress  = flag.String("address", "localhost:1234", "Application server address")
	opLogLevel = flag.String("loglevel", "debug", "Log level: debug|info|warn|error|fatal")
	opStatic   = flag.String("static", "../staticfs/static", "Path for static files")
)

// this package logger
var log = logger.Default()

func main() {

	cfg := gwi.ServerConfig{
		Address:    *opAddress,
		Static:     *opStatic,
		MaxClients: 1,
	}
	server.Start(&cfg, clientConn)
}

func clientConn(client *gwi.Client, open bool) {

	if open {
		log.Info("Connection with client opened")
		buttonStart(client)
	} else {
		log.Info("Connection with client closed")
	}
}
