package main

import (
	"time"

	"gitlab.com/leonsal/gwi"
)

var buttonLog func(string)

func buttonStart(c *gwi.Client) {

	err := c.Ui(buttonConfig)
	if err != nil {
		log.Error("%s", err)
		return
	}
	c.Set("document.title", "GWIDemo")

	buttonLog = func(msg string) {
		ts := time.Now().Format("15:04:05")
		c.View("tlist").Call("add", gwi.Obj{"time": ts, "msg": msg}, 0)
	}
}

var buttonConfig = gwi.Obj{
	"view": "layout",
	"rows": gwi.Array{
		gwi.Obj{
			"view": "toolbar",
			"cols": gwi.Array{
				gwi.Obj{
					"id":    "b1",
					"view":  "button",
					"value": "B1",
					"on": gwi.Obj{
						"onItemclick": button1Click,
						"onKeyPress":  button1Key,
					},
				},
				gwi.Obj{"id": "b2", "view": "button", "value": "B2", "click": button2Click},
				gwi.Obj{"id": "b3", "view": "button", "value": "B3 attach B5", "click": button3Click},
				gwi.Obj{"id": "b4", "view": "button", "value": "B4 detach B5", "click": button4Click},
				gwi.Obj{"id": "b5", "view": "button", "value": "B5"},
				gwi.Obj{"id": "b6", "view": "button", "value": "Form", "click": button6Click},
			},
		},
		gwi.Obj{
			"view":     "list",
			"id":       "tlist",
			"template": "#time#  #msg#",
		},
	},
}

func button1Click(c *gwi.Client, ev *gwi.MsgEvent) {

	buttonLog("B1 clicked")
}

func button1Key(c *gwi.Client, ev *gwi.MsgEvent) {

	buttonLog("B1 key pressed")
}

func button2Click(c *gwi.Client, ev *gwi.MsgEvent) {

	buttonLog("B2 clicked")
}

func button3Click(c *gwi.Client, ev *gwi.MsgEvent) {

	c.AttachEvent("b5", "onItemClick", button5Click, "b5handler")
	buttonLog("B3 clicked -> attach B5")
}

func button4Click(c *gwi.Client, ev *gwi.MsgEvent) {

	c.DetachEvent("b5", "b5handler")
	buttonLog("B4 clicked -> detach B5")
}

func button5Click(c *gwi.Client, ev *gwi.MsgEvent) {

	buttonLog("B5 clicked")
}

func button6Click(c *gwi.Client, ev *gwi.MsgEvent) {

	buttonLog("B5 clicked")
	formStart(c)
}

//func buttonClick6(c *gwi.Client, ev *gwi.MsgEvent) {
//
//	fmt.Println("onClick6", ev.Vid)
//	button := c.View(ev)
//	button.Call("setValue", "Clicked")
//	button.Call("disable")
//}
