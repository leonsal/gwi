module gitlab.com/leonsal/gwi/gwidemo

go 1.16

replace gitlab.com/leonsal/gwi => ../

require (
	github.com/webview/webview v0.0.0-20210330151455-f540d88dde4e
	gitlab.com/leonsal/gwi v0.8.2
)
