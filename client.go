package gwi

import (
	"fmt"
	"strconv"

	ws "github.com/gorilla/websocket"
)

type Obj map[string]interface{}
type Array []interface{}

// Type for all event handlers functions
type EventHandler func(*Client, *MsgEvent)

// Type for all promise handlers functions
type PromiseHandler func(*Client, string)

type Client struct {
	conn       *ws.Conn                 // Socket connection
	nextId     int                      // Next message id to use
	chanResp   chan msgResp             // Channel for received responses from client
	nextVid    int                      // Next view id to use
	nextHid    int                      // Next handler id to use
	nextPid    int                      // Next promise id to use
	viewEvents map[string]*viewHandlers // Maps view id to view event handlers
	promiseMap map[int]PromiseHandler   // Maps promise id to its handler
}

type ViewProxy struct {
	client *Client
	vid    string
}

// Type viewHandlers contains all events associated with a view
type viewHandlers struct {
	config   map[string]EventHandler // Maps handlers in view config to event handlers
	attached map[string]EventHandler // Maps attached handlers
}

func newClient(conn *ws.Conn) *Client {

	c := new(Client)
	c.conn = conn
	c.nextId = 1
	c.nextVid = 1
	c.nextHid = 1
	c.nextPid = 1
	c.chanResp = make(chan msgResp)
	c.viewEvents = make(map[string]*viewHandlers)
	c.promiseMap = make(map[int]PromiseHandler)
	return c
}

func (c *Client) Ui(ui Obj) error {

	var scanObject func(obj Obj) error
	var scan func(cont interface{}) error
	lastVid := ""

	scanObject = func(obj Obj) error {

		// If object is a view, generates or get its id
		if obj["view"] != nil {
			value, ok := obj["id"]
			if !ok {
				lastVid = fmt.Sprintf("gwiID:%d", c.nextVid)
				obj["id"] = lastVid
				c.nextVid++
				// Checks if supplied id is a string
			} else {
				lastVid, ok = value.(string)
				if !ok {
					return fmt.Errorf("View id must be a string")
				}
			}
			// Checks if id is unique
			if _, ok := c.viewEvents[lastVid]; ok {
				return fmt.Errorf("View id:%s is not unique", lastVid)
			}
			c.viewEvents[lastVid] = &viewHandlers{
				config:   make(map[string]EventHandler),
				attached: make(map[string]EventHandler),
			}
		}
		// Process event handlers
		for k, v := range obj {
			//fmt.Printf("Key:%v Value:%T\n", k, v)
			// Process event handler functions
			evfunc, ok := v.(func(*Client, *MsgEvent))
			if ok {
				handlers := c.viewEvents[lastVid]
				hid := len(handlers.config) + 1
				obj[k] = fmt.Sprintf("gwi.eventHandler%d", hid)
				handlers.config[strconv.Itoa(hid)] = evfunc
				continue
			}
			// Scan possible containers recursively
			err := scan(v)
			if err != nil {
				return err
			}
		}
		return nil
	}

	scan = func(cont interface{}) error {
		// Checks if element is an array
		arr, ok := cont.(Array)
		if ok {
			for _, el := range arr {
				err := scan(el)
				if err != nil {
					return err
				}
			}
			return nil
		}
		// Checks if element is an Object
		obj, ok := cont.(Obj)
		if ok {
			return scanObject(obj)
		}
		return nil
	}

	config := copyObj(ui)
	err := scan(config)
	if err != nil {
		return err
	}

	//fmt.Printf("CONFIG:%+v\n:", config)
	//c.Call("webix.ui", true, "", 0, config, "root")
	c.call("webix.ui", true, "", 0, config)
	return nil
}

// AttachEvent attaches a handler to an inner event of the specified view
// Returns the id of the attached handler which can be used to detach the event handler.
func (c *Client) AttachEvent(vid string, evname string, handler EventHandler, hid string) (string, error) {

	// Get view handlers
	viewHandlers := c.viewEvents[vid]
	if viewHandlers == nil {
		err := fmt.Errorf("Invalid view id:%s", vid)
		log.Error("%s", err)
		return "", err
	}

	// If handler id not supplied, generates
	if len(hid) == 0 {
		hid = strconv.Itoa(c.nextHid)
		c.nextHid++
	} else {
		if _, ok := viewHandlers.attached[hid]; ok {
			err := fmt.Errorf("Handler id:%s for view:%s is not unique", hid, vid)
			log.Error("%s", err)
			return "", err
		}
	}
	hname := "gwi.sendEvent," + hid

	// Call attachEvent in client
	res, err := c.call("attachEvent", true, vid, 0, evname, hname, hid)
	if err != nil {
		return "", err
	}
	log.Debug("attachedEvent id:%s", res)

	// Save handler id for the view
	viewHandlers.attached[hid] = handler

	return hid, nil
}

func (c *Client) DetachEvent(vid string, hid string) error {

	// Get view handlers
	viewHandlers := c.viewEvents[vid]
	if viewHandlers == nil {
		err := fmt.Errorf("Invalid view id:%s", vid)
		log.Error("%s", err)
		return err
	}

	// Checks if handler id is valid
	if _, ok := viewHandlers.attached[hid]; !ok {
		err := fmt.Errorf("Invalid handler id:%s for view:%s", hid, vid)
		log.Error("%s", err)
		return err
	}

	// Sends call to detachEvent
	_, err := c.call("detachEvent", true, vid, 0, hid)
	if err != nil {
		return err
	}
	delete(viewHandlers.attached, hid)
	return nil
}

// Call calls a client global function or method of global object
// with the specified parameters and returns the result encoded in JSON.
func (c *Client) Call(fn string, params interface{}) (string, error) {

	return c.call(fn, true, "", 0, params)
}

// Set sets value of property of client's global object
func (c *Client) Set(prop string, value interface{}) error {

	return c.set(prop, true, "", value)
}

// Get get value of property of client's global object
func (c *Client) Get(prop string) (string, error) {

	return c.get(prop, true, "")
}

// Promise call a client function with returns a promise and sets the handler
// which will be called when the promise resolves.
func (c *Client) Promise(fn string, params interface{}, handler PromiseHandler) error {

	pid := 0
	if handler != nil {
		pid = c.nextPid
		c.promiseMap[pid] = handler
	}
	_, err := c.call(fn, true, "", pid, params)
	return err
}

// View creates and returns a ViewProxy object which can be used to
// access its methods and properties
func (c *Client) View(idev interface{}) ViewProxy {

	v := ViewProxy{client: c}
	vid, ok := idev.(string)
	if ok {
		v.vid = vid
		return v
	}
	ev, ok := idev.(*MsgEvent)
	if ok {
		v.vid = ev.Vid
		return v
	}
	panic("Must be view id or Event")
}

// Call calls a method of the view and returns a result and an error
func (v ViewProxy) Call(fn string, params ...interface{}) (string, error) {

	return v.client.call(fn, true, v.vid, 0, params...)
}

// Set sets the value of the specified property of the view
func (v ViewProxy) Set(prop string, value interface{}) error {

	return v.client.set(prop, false, v.vid, value)
}

// Get returns the value of the specified property of the view
func (v ViewProxy) Get(prop string) (string, error) {

	return v.client.get(prop, true, v.vid)
}

func (c *Client) Close() {

}

func (c *Client) call(fn string, sync bool, vid string, pid int, params ...interface{}) (string, error) {

	msg := msgAny{}
	msg.Type = msgCallType
	msg.Sync = sync
	msg.Body = msgCall{
		Vid:    vid,
		Pid:    pid,
		Func:   fn,
		Params: params,
	}
	resp, err := c.send(msg)
	if err != nil {
		log.Error("Error in Call:%s/%s response:%v", fn, vid, err)
	}
	return resp, err
}

func (c *Client) set(prop string, sync bool, vid string, value interface{}) error {

	msg := msgAny{}
	msg.Type = msgSetType
	msg.Sync = sync
	msg.Body = msgSet{
		Vid:   vid,
		Prop:  prop,
		Value: value,
	}
	_, err := c.send(msg)
	if err != nil {
		log.Error("Error in Set:%s/%s response:%v", prop, value, err)
	}
	return nil
}

func (c *Client) get(prop string, sync bool, vid string) (string, error) {

	msg := msgAny{}
	msg.Type = msgGetType
	msg.Sync = sync
	msg.Body = msgGet{
		Vid:  vid,
		Prop: prop,
	}
	resp, err := c.send(msg)
	if err != nil {
		log.Error("Error in Get:%s/%s response:%v", prop, vid, err)
	}
	return resp, err
}

func (c *Client) dispatchEvent(event *MsgEvent) {

	//fmt.Printf("DISPATCH:%+v\n", event)
	// Checks if it is a promise event
	if event.Pid > 0 {
		handler := c.promiseMap[event.Pid]
		if handler == nil {
			log.Error("Promise handler not found for pid:%d", event.Pid)
			return
		}
		delete(c.promiseMap, event.Pid)
		go handler(c, event.Data)
		return
	}

	// Get associated view handlers
	viewHandlers := c.viewEvents[event.Vid]
	if viewHandlers == nil {
		log.Error("Event with invalid view:%v", event.Vid)
		return
	}
	//log.Debug("Client view handlers:%+v", viewHandlers)

	// Look for handler for received handler id
	handler := viewHandlers.config[event.Hid]
	if handler == nil {
		handler = viewHandlers.attached[event.Hid]
		if handler == nil {
			log.Error("Event with invalid handler:%s for view:%s", event.Hid, event.Vid)
			return
		}
	}
	// Calls user handler in a goroutine
	go handler(c, event)
}

func copyObj(o Obj) Obj {

	var copyValue func(v interface{}) interface{}
	copyValue = func(v interface{}) interface{} {
		obj, ok := v.(Obj)
		if ok {
			return copyObj(obj)
		}
		arr, ok := v.(Array)
		if ok {
			c := Array{}
			for _, el := range arr {
				c = append(c, copyValue(el))
			}
			return c
		}
		return v
	}

	c := Obj{}
	for k, v := range o {
		c[k] = copyValue(v)
	}
	return c
}
