package webview_ext

import (
	"net/http"
	"os"

	"github.com/webview/webview"
	"gitlab.com/leonsal/gwi"
	"gitlab.com/leonsal/gwi/logger"
)

var log = logger.Default()

func Start(cfg *gwi.ServerConfig, h func(*gwi.Client, bool)) error {

	// Creates HTTP server
	server := &http.Server{
		Addr: cfg.Address,
	}

	// Sets static file server
	log.Info("Using external static filesystem:%s", cfg.Static)
	hfs := http.FS(os.DirFS(cfg.Static))
	http.Handle("/", http.FileServer(hfs))

	// Creates GWI handler
	handler, err := gwi.New(&gwi.Config{MaxClients: cfg.MaxClients}, h)
	if err != nil {
		return err
	}
	http.Handle("/gwi", handler)

	go func() {
		log.Info("listening on:%s", cfg.Static)
		err := server.ListenAndServe()
		if err != nil {
			log.Error("%s", err)
		}
	}()

	// Creates webview
	log.Info("Creating webview")
	debug := true
	w := webview.New(debug)
	defer w.Destroy()
	w.SetTitle("")
	w.SetSize(800, 600, webview.HintNone)

	// Starts webview
	scriptPath := "http://" + cfg.Static
	w.Navigate(scriptPath)
	w.Run()
	return nil
}
