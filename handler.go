package gwi

import (
	"encoding/json"
	"errors"
	"net/http"
	"sync"
	"time"

	ws "github.com/gorilla/websocket"
	"gitlab.com/leonsal/gwi/logger"
)

type ServerConfig struct {
	Address    string
	Static     string
	MaxClients int
}

type Config struct {
	MaxClients int
}

type Handler struct {
	config      Config
	clientMap   map[*ws.Conn]*Client // Maps client connection to its state
	userHandler func(*Client, bool)
	mut         sync.Mutex
}

const (
	msgCallType  = "call"  // call function/method
	msgSetType   = "set"   // set property value
	msgGetType   = "get"   // get property value
	msgEventType = "event" // client event
	msgRespType  = "resp"  // client response
)

// Header for all message types
type msgHeader struct {
	Id   int    `json:"id"`
	Type string `json:"type"`
	Sync bool   `json:"sync,omitempty"`
}

// Type for all messages
type msgAny struct {
	msgHeader
	Body interface{} `json:"body"`
}

// Body for remote call message
type msgCall struct {
	Vid    string      `json:"vid"`
	Pid    int         `json:"pid"`
	Func   string      `json:"func"`
	Params interface{} `json:"params"`
}

// Body for remote property set message
type msgSet struct {
	Vid   string      `json:"vid"`
	Prop  string      `json:"prop"`
	Value interface{} `json:"value"`
}

// Body for remote get property message
type msgGet struct {
	Sync bool   `json:"sync"`
	Vid  string `json:"vid"`
	Prop string `json:"prop"`
}

// Body for client response from previous call,set or get message
type msgResp struct {
	Error string `json:"error"`
	Data  string `json:"data"`
}

// Body for client event
type MsgEvent struct {
	Vid  string `json:"vid"`  // Associated view id
	Hid  string `json:"hid"`  // Associated handler id
	Pid  int    `json:"pid"`  // Associate promise id
	Type string `json:"type"` // Possible event type
	Data string `json:"data"` // Optional event data
}

var log = logger.Default()

// New creates and returns a new handler
func New(cfg *Config, user func(*Client, bool)) (*Handler, error) {

	h := new(Handler)
	h.config = *cfg
	h.clientMap = make(map[*ws.Conn]*Client)
	h.userHandler = user
	return h, nil
}

// ServeHTTP implements the http.Handler interface and is called by the http server
// in a goroutine when a client connection is received.
func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	// Upgrades HTTP connection to WebSocket connection
	var upgrader = ws.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Error("Error in socket upgrade:", err)
		return
	}

	// Creates new Client and stores it in the client map
	client := newClient(conn)
	h.mut.Lock()
	h.clientMap[conn] = client
	h.mut.Unlock()
	log.Debug("Socket received connection from:%s", conn.RemoteAddr())

	// Informs user of client connection opened in another goroutine
	// as the user will probably starts sending messages to client
	go h.userHandler(client, true)

	// Block waiting for messages from Client
	for {
		mt, message, err := conn.ReadMessage()
		if err != nil {
			log.Error("Socket error:%v", err)
			break
		}
		h.decode(client, mt, message)
	}

	// Informs user of client connection closed
	go h.userHandler(client, false)

	log.Debug("Socket connection with client:%v closed", conn.RemoteAddr())
	h.mut.Lock()
	defer h.mut.Unlock()
	delete(h.clientMap, conn)
}

// decode decodes received messages (responses and events from client)
func (h *Handler) decode(c *Client, mtype int, mdata []byte) {

	// Only text messages are supported
	if mtype != ws.TextMessage {
		log.Fatal("Received message type: %v not supported", mtype)
		return
	}

	log.Debug("RX:%v", string(mdata))
	// Decodes message from JSON
	var msg msgAny
	err := json.Unmarshal(mdata, &msg)
	if err != nil {
		log.Error("Handler.decode: %v", err)
		return
	}

	// Checks if message is an event from client
	if msg.Type == msgEventType {
		body := msg.Body.(string)
		var event MsgEvent
		err := json.Unmarshal([]byte(body), &event)
		if err != nil {
			log.Error("Handler.decode: %v", err)
			return
		}
		c.dispatchEvent(&event)
		return
	}

	// Message is a response from client
	if msg.Type == msgRespType {
		body := msg.Body.(string)
		var resp msgResp
		err := json.Unmarshal([]byte(body), &resp)
		if err != nil {
			log.Error("Handler.decode: %v", err)
			return
		}
		select {
		case c.chanResp <- resp:
		default:
			log.Error("Client response channel full")
		}
		return
	}

	log.Error("Received invalid message type:%s", msg.Type)
}

//func (c *Client) send(mtype string, sync bool, vid string, data interface{}) (string, error) {
func (c *Client) send(msg msgAny) (string, error) {

	msg.Id = c.nextId
	encoded, err := json.Marshal(msg)
	if err != nil {
		log.Fatal("Error encoding message:%v -> %v", msg, err)
	}

	// Sends message
	err = c.conn.WriteMessage(ws.TextMessage, []byte(encoded))
	if err != nil {
		return "", err
	}
	c.nextId++
	log.Debug("TX:%+v", msg)

	// If message is not synchronous, nothing else to do
	if !msg.Sync {
		return "", nil
	}

	// Waits for response or timeout
	select {
	case resp := <-c.chanResp:
		if len(resp.Error) > 0 {
			return "", errors.New(resp.Error)
		}
		return resp.Data, nil
	case <-time.After(100 * time.Millisecond):
		err := errors.New("Timeout waiting for response")
		log.Error("%s", err)
		return "", err
	}
}
