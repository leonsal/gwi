"use strict"

var gwi = {

    // Websocket readyStates:
    CONNECTING: 0,
    OPEN:       1,
    CLOSING:    2,
    CLOSED:     3,

    // Log flag
    LOG:    true,

    // Logs to console.log if LOG==true
    log: function() {

        if (!gwi.LOG) {
            return;
        }
        const args = ["GWI:"];
        for (let i = 0; i < arguments.length; i++) {
            args.push(arguments[i]);
        }
        console.log.apply(null, args);
    },

    // Socket class
    Socket: class {

        // Creates a new Socket object
        // relurl:  relative server url to connect <string>
        // retry:   keep retrying connection if true <bool>
        // cb:      Callback to inform of connection events
        constructor(relurl, retry, cb) {

            this._relurl = relurl;
            this._retry = retry;
            this._cb    = cb;
            this._socket = null;
            this._nextid = 1;
            this._handlers = {};
            this._forceClose = false;
            this._retryTimeout = 1000;
            this._timeoutId = null;
        }

        // Starts connection with the server if not already connected
        connect() {

            // If connection already active or retrying, does nothing
            if (this._socket && this._socket.readyState != gwi.CLOSED) {
                return;
            }
            this._forceClose = false;
            
            // Open web socket connection with server using the supplied relative URL
            const url = 'ws://' + document.location.host + "/" + this._relurl;
            this._socket = new WebSocket(url);

            // Sets event handlers
            this._socket.onopen    = ev => this._onOpen(ev);
            this._socket.onerror   = ev => this._onError(ev);
            this._socket.onclose   = ev => this._onClose(ev);
            this._socket.onmessage = ev => this._onMessage(ev);
        }

        // Closes the connection
        close() {
            if (!this._socket || this._socket.readyState != gwi.CLOSED) {
                return;
            }
            this._socket.close();
            this._forceClose = true
        }

        // Returns the socket ready state
        status() {
            if (!this._socket) {
                return CLOSED;
            }
            return this._socket.readyState;
        }

        // Sends message
        send(id, type, body) {

            // Checks if connection is open
            if (this._socket.readyState != gwi.OPEN) {
                return "connection is not open";
            }

            // If message id not supplied, use the next id
            if (!id) {
                id = this._nextid;
                this._nextid++;
            }

            // Builds message envelope
            // The message body is sent as a string to be decoded by
            // server's specific message type decoder
            const msg = {
                id:     id,
                type:   type,
                body:   JSON.stringify(body),
            };

            // Encodes message to JSON and sends is as text
            const text = JSON.stringify(msg);
            this._socket.send(text);
            gwi.log("TX:", msg);
            return null
        }

        // Register received message handler
        register(type, cb) {

            this._handlers[type] = cb
        }

        _onOpen(ev) {
            this._inform('open');
        }

        _onError(ev) {
            gwi.log("onError");
        }

        _onClose(ev) {

            if (this._forceClose || !this._retry) {
                this._inform('close');
                return;
            }
            this._inform('retry');
            this._timeoutId = setTimeout(()=>this.connect(), this._retryTimeout);
        }

        // Called when a message is received from the socket.
        _onMessage(ev) {

            const msg = JSON.parse(ev.data);
            // Checks message fields
            if (msg.id == undefined || msg.type == undefined) {
                gwi.log('Received invalid message');
                return;
            }

            const handler = this._handlers[msg.type];
            if (!handler) {
                gwi.log('Received message without registered handler', msg);
                return;
            }
            gwi.log("RX:", msg);
            handler(msg);
        }

        _inform(msg) {

            if (!this._cb) {
                return;
            }
            this._cb(msg);
        }

    },

    // Global socket
    socket:  null,

    start: function() {

        gwi.socket = new gwi.Socket("gwi", false, msg => {
            gwi.log("WebSocket: ", msg);
        });

        gwi.socket.register("call", gwi.call)
        gwi.socket.register("get",  gwi.get)
        gwi.socket.register("set",  gwi.set)
        gwi.socket.connect();
    },


    // Event handlers specified in views config object
    eventHandler1: function() {
        gwi.sendEvent.call(null, this, "1", arguments);
    },
    
    eventHandler2: function() {
        gwi.sendEvent.call(null, this, "2", arguments);
    },
    
    eventHandler3: function() {
        gwi.sendEvent.call(null, this, "3", arguments);
    },

    // Send widget event to server
    // view:   webix view object <object>
    // hid:    handler id <string>
    // evargs: event arguments  <arguments>
    sendEvent: function(view, hid, evargs) {

        // Build array of event parameters from evargs (arguments)
        // Do not include full objects
        const args = [];
        let i = 0;
        for (let j = 0; j < evargs.length; j++) {
            if (typeof(evargs[j]) == "object") {
                continue;
            }
            args[i] = evargs[j];
            i++;
        }

        // Encodes event data as JSON string
        const body = {
            vid:    view.config.id,
            hid:    hid,
            data:   JSON.stringify(args),
        };
        gwi.socket.send(null, "event", body);
    },

    // Call function or method from view
    call: function(msg) {

        const transform = function(p) {
            // If parameter is a string withthe name of a gwi local function
            // in the form: "gwi.<name>,p1,p2,..pN"
            // returns a new function which will call the gwi local
            // function with the specified constant parameters
            if (typeof(p) == "string" && p.startsWith("gwi.")) {
                let parts = p.split(",");
                if (parts.length == 1) {
                    return p;
                }
                let fn = window.Function("return " + parts[0])();
                return function() {
                    fn.call(null, this, parts[1], arguments);
                }
            }
            return p;
        }

        // Get function to execute
        let fn;
        let view;
        if (!msg.body.vid) {
            fn = window.Function("return " + msg.body.func)();
        } else {
            view = $$(msg.body.vid);
            if (!view) {
                gwi.sendResp(msg, "Invalid view id:"+msg.body.vid);
                return;
            }
            fn = view[msg.body.func];
            if (!fn) {
                gwi.sendResp(msg, "Invalid view method:"+msg.body.vid+"."+msg.body.func);
                return;
            }
        }

        // Transform the function call parameters if necessary
        const params = [];
        if (msg.body.params) {
            for (let i=0; i < msg.body.params.length; i++) {
                let res = transform(msg.body.params[i]);
                params[i] = res;
            }
        }

        // Try to execute function with supplied parameters
        let res;
        try {
            res = fn.apply(view, params);
        } catch (error) {
            gwi.sendResp(msg, error.name+":"+error.message);
            return;
        }

        // If function call returns a promise, sends event when promise is resolved.
        if (msg.body.pid > 0) {
            res.then((data) => {
                gwi.sendPromiseEvent(msg.body.pid, data);
            });
        }

        const result = gwi.getResult(res);
        gwi.sendResp(msg, null, result);
    },

    // Get and returns property value
    get: function(msg) {

        let fn;
        if (!msg.body.vid) {
            fn = window.Function("return " + msg.body.prop);
        } else {
            const view = window.Function("return $$(${msg.vid})");
            if (!view) {
                gwi.sendResp(msg, "Invalid view id:" + msg.vid);
                return;
            }
            fn = window.Function("return view." + msg.body.prop);
        }
        try {
            fn();
            gwi.sendResp(msg, null, null);
        } catch (error) {
            gwi.sendResp(msg, error.name+":"+error.message);
        }
    },

    // Sets property
    set: function(msg) {

        let js;
        if (!msg.body.vid) {
            if (typeof(msg.body.value) == "string") {
                js = `${msg.body.prop}="${msg.body.value}";`;
            } else {
                js = `${msg.body.prop}=${msg.body.value};`;
            }
        } else {
            const view = window.Function("return $$(${msg.vid})");
            if (!view) {
                gwi.sendResp(msg, "Invalid view id:" + msg.vid);
                return;
            }
            if (typeof(msg.body.value) == "string") {
                js = `view.${msg.body.prop}="${msg.body.value}";`;
            } else {
                js = `view.${msg.body.prop}=${msg.body.value};`;
            }
        }
        try {
            window.Function(js)();
            gwi.sendResp(msg, null, null);
        } catch (error) {
            gwi.sendResp(msg, error.name+":"+error.message);
        }
    },

    // Sends event informing of promise resolved
    sendPromiseEvent: function(pid, data) {

        const body = {pid:pid, data:JSON.stringify(data)};
        gwi.socket.send(null, "event", body);
        gwi.log("promise resolved sent");
    },

    // Sends response to server from a function call, set or get
    // The data is allways sent as as string to be decoded by specific
    // server decoder depending on the expected response.
    // The response is only sent if the request message set the "sync" flag.
    // msg:   Request message from server <object>
    // error: Error <string>|<null>
    // data:  Response <string>|<null>
    sendResp: function(msg, error, data) {

        if (!msg.sync) {
            return;
        }
        gwi.socket.send(msg.id, "resp", {error:error, data:JSON.stringify(data)});
    },

    // Transforms the result from a function call to the data to send to server
    getResult: function(fnret) {

        // If is an array, process each element recursively
        if (webix.isArray(fnret)) {
            const res = [];
            for (let i = 0; i < fnret.length; i++) {
                res[i] = gwi.getResult(fnret[i]);
            }
            return res;
        }

        // Object
        if (typeof(fnret) == "object") {
            // If it is a webix view, returns only the view id.
            if (fnret["$view"] && fnret.config) {
                return fnret.config.id;
            }
            // If it is a promise, returns nothing
            if (fnret.promise && fnret.resolve) {
                return null;
            }
        }
        return fnret;
    }
};

gwi.start();


